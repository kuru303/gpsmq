package br.com.searchtecnologia.gps.gpsartemisjms;

import org.apache.activemq.artemis.api.core.TransportConfiguration;
import org.apache.activemq.artemis.core.remoting.impl.netty.NettyAcceptorFactory;
import org.apache.activemq.artemis.core.remoting.impl.netty.NettyConnectorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.boot.SpringApplication.run;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.artemis.ArtemisConfigurationCustomizer;
import org.springframework.boot.autoconfigure.jms.artemis.ArtemisProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GpsArtemisJmsApplication {

    @Autowired
    private ArtemisProperties artemisProperties;

    public static void main(String[] args) {
        run(GpsArtemisJmsApplication.class, args);
    }

    @Bean
    public ArtemisConfigurationCustomizer artemisConfigurationCustomizer() {
        return configuration -> {
            try {
                configuration.addAcceptorConfiguration("netty", "tcp://" + artemisProperties.getHost() + ":" + artemisProperties.getPort());
                // configuration.addConnectorConfiguration("nettyConnector", new TransportConfiguration(NettyConnectorFactory.class.getName()));
                // configuration.addAcceptorConfiguration(new TransportConfiguration(NettyAcceptorFactory.class.getName()));
    
            } catch (Exception e) {
                throw new RuntimeException("Failed to add netty transport acceptor to artemis instance");
            }
        };
    }
}
