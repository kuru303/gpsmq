package br.com.searchtecnologia.gps.gpsartemisjms.jms;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Service
public class JmsConsumer {

    Logger log = getLogger(JmsConsumer.class);

    @JmsListener(destination = "${spring.jms.template.default-destination}")
    public void receive(String message) {
	log.info("Received message='{}'", message);
    }
}
