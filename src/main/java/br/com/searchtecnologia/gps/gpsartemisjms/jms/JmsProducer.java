package br.com.searchtecnologia.gps.gpsartemisjms.jms;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class JmsProducer {

  Logger log = getLogger(JmsProducer.class);

  @Autowired
  private JmsTemplate jmsTemplate;

  public void send(String message) {
    jmsTemplate.convertAndSend(message);
    log.info("Sent message='{}'", message);
  }
}